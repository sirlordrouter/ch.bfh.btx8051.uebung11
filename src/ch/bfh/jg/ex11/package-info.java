/**
 * Berner Fachhochschule</br>
 * Medizininformatik BSc</br>
 * Modul 8051-HS2012</br>
 * 
 *<p>Contains the main classes for this projects</p>
 *
 * @author Johannes Gnaegi, gnaegj1@bfh.ch
 * @version 25.09.2012
 */
package ch.bfh.jg.ex11;