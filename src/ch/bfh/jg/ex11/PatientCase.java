package ch.bfh.jg.ex11;
import ch.bfh.jg.util.*;

/**
 * Berner Fachhochschule</br>
 * Medizininformatik BSc</br>
 * Modul 8051-HS2012</br>
 * 
 *<p><b>Mutable Class</b></p>
 *<p>
 *This Class describes an Case of a Patient. With the given parameters the Case is opend.
 *</p>
 *
 * @author Johannes Gnaegi, gnaegj1@bfh.ch
 * @version 25.09.2012
 */
public class PatientCase {

	private final long aCaseID;
	private final String anAdmReason;
	private final String anAdmDate;
	//Cohesion?
	private final int aSize;
	//Cohesion?
	private final int aWeight; 
	//Cohesion?
	private final String DateOfBirth;
	//Cohesion?
	private final char aGender; 
	//Cohesion?
	private final BodyDataBasic bda; 
	//Cohesion?
	private final DeltaTime aDeltaTimeBirthday; 
	 
	private final int aAge;
	private final FormattedDate aAgeFormatted; 
	
	private DueDate aDueDate;
	//private Address aAddress;
	
	/**
	 * onstructs a PatientCase object on the base of different parameters.
	 * 
	 * @param aCaseID Case ID
	 * @param anAdmReason Reason of the Administration
	 * @param anAdmDate Date of the admission (Format: dd.mm.yyyy).
	 * @param aSize Size of the patient in cm (should be: > 0, reasonable > 140).
	 * @param aWeight Weight of the patient in kg (should be: > 0).
	 * @param aDateOfBirth Date of birth of the patient (Format: dd.mm.yyyy).
	 * @param aGender Gender of the patient (should be: m, f).
	 */
	public PatientCase(long aCaseID, String anAdmReason, 
			String anAdmDate, int aSize, int aWeight, String aDateOfBirth, char aGender)
	{
		//TODO: In Testklasse auslagern. 
		assert aCaseID != 0; 
		assert !anAdmReason.equalsIgnoreCase("");
		assert !anAdmDate.equalsIgnoreCase("") 
		       && anAdmDate.equalsIgnoreCase("(0[1-9]|[12][0-9]|3[01])\\.(0[1-9]|1[012])\\.(19|20)\\d\\d"); 
		assert aSize != 0;
		assert aWeight != 0; 
		assert !anAdmDate.equalsIgnoreCase("") 
		       && anAdmDate.equalsIgnoreCase("(0[1-9]|[12][0-9]|3[01])\\.(0[1-9]|1[012])\\.(19|20)\\d\\d");
		assert aGender == 'm' 
				|| aGender == 'M'
				|| aGender == 'f'
				|| aGender == 'F'; 
		
		this.aCaseID = aCaseID;
		this.anAdmReason = anAdmReason;
		this.anAdmDate = anAdmDate;
		this.aSize = aSize;
		this.aWeight = aWeight;
		this.DateOfBirth = aDateOfBirth;
		this.aGender = aGender;	
		this.aAgeFormatted = new FormattedDate(aDateOfBirth); 
		this.aDeltaTimeBirthday = new DeltaTime(aAgeFormatted.day, aAgeFormatted.month, aAgeFormatted.year);	
		this.aAge = (int) aDeltaTimeBirthday.getDeltaTimeInYears(); 		
		this.bda = new BodyDataBasic(aSize, aWeight, aGender, aAge);
	}
	
	/**
	 * Returns the admission date (Format: dd.mm.yyyy).
	 * @return admission date (Format: dd.mm.yyyy).
	 */
	public String getAdmissionDate()
	{
		return anAdmDate;
	}
	
	/**
	 * Returns the admission reason.
	 * @return admission reason.
	 */
	public String getAdmissionReason()
	{
		return anAdmReason;
	}
	
	/**
	 * Returns the age of the patient in years, months and days at the admission.
	 * @return FormattedDate of Birthday.
	 */
	public FormattedDate getAge()
	{
		return aAgeFormatted; 
	}
	
	/**
	 * Returns the age of the patient in years at the admission.
	 * @return age of the patient in years
	 */
	public int getAgeInYears()
	{
		return aAge;
	}
	
	/**
	 * Returns the Body Mass Index (BMI) on the base of the weight and the size at the admission.
	 * @return Body Mass Index (BMI)
	 */
	public double getBMI()
	{
		return bda.getBMI(); 
	}
	
	/**
	 * Returns the Body Surface Area (BSA) in m<sup>2</sup> on the base of the age, 
	 * size and weight at the admission.
	 * @return Body Surface Area (BSA) in m<sup>2</sup>
	 */
	public double getBSA()
	{
		return bda.getBSA(); 
	}
	
	/**
	 * Returns all case data of this case.
	 * @return all case data
	 */
	public String getCaseData()
	{		
		return "\t\nCase ID: " + getCaseID()
				+ "\t\nAdministration Date: " + getAdmissionDate()
				+ "\t\nAdministration Reason: " + getAdmissionReason()
				+ "\t\nSize: " + getSize()
				+ "\t\nWeight: " + getWeight()
				+ "\t\nDate of Birth: " + DateOfBirth
				+ "\t\nGender: " + aGender; 
	}
	
	/**
	 * Returns the case ID.
	 * @return case ID
	 */
	public long getCaseID()
	{
		return aCaseID;
	}
	
	/**
	 * Returns the expected due date according to the "standard rule" 
	 * on the base of the date of the last menstrual period (Format: dd.mm.yyyy).
	 * @return due date according to the "standard rule"
	 */
	public String getDueDate()
	{
		String result = "No DueDate available."; 
		if(aDueDate != null) {
			result = aDueDate.getDueDate();
		}
		
		return result; 
	}
	
	/**
	 * Returns the expected due date according to Naegele's rule 
	 * on the base of the date of the last menstrual period (Format: dd.mm.yyyy).
	 * @return due date according to Naegele's rule
	 */
	public String getDueDateNaegele()
	{
		String result = "No DueDate available."; 
		if(aDueDate != null) {
			result = aDueDate.getDueDateNaegele();
		}
		
		return result; 
	}
	
	/**
	 * Returns the size of the patient in cm at the admission.
	 * @return size of the patient in cm
	 */
	public int getSize()
	{
		return aSize; 
	}
	
	/**
	 * Returns the weight of the patient in kg at the admission.
	 * @return weight of the patient in kg
	 */
	public double getWeight()
	{
		return aWeight;
	}
	
	/**
	 * Sets the date of the last menstrual period (Format: dd.mm.yyyy).
	 * @param aDate Date of last menstrual period.
	 */
	public void setDateOfLastMenstrualPeriod(String aDate)
	{
		FormattedDate periodDate = new FormattedDate(aDate);
		aDueDate = new DueDate(periodDate.day, periodDate.month, periodDate.year);
	}
	
}
