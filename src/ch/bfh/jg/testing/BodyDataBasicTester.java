package ch.bfh.jg.testing;

import ch.bfh.jg.util.BodyDataBasic;

/**
 * Berner Fachhochschule</p>
 * Medizininformatik BSc</p>
 * Modul 8051-HS2012</p>
 * 
 * @author Johannes Gnaegi, gnaegj1@bfh.ch
 * @version 08.10.2012
 */
public class BodyDataBasicTester {

	/**
	 * Testet die Klasse BodyDataBasic. 
	 */
	public static void main(String[] args) {


		//24 jaehriger Mann, 1.80m, 75 kg
		BodyDataBasic bdaNormalM24 = new BodyDataBasic(180, 75, 'm', 24);
		//24 jaehrige Frau, 1.75m, 55 kg
		BodyDataBasic bdaNormalF24 = new BodyDataBasic(175, 55, 'f', 24);
		//20 jaehriger Mann, 1.80m, 74 kg
		BodyDataBasic bdaNormalM20 = new BodyDataBasic(180, 74, 'm', 20);
		//Kind 5, 1.1m, 20 kg
		BodyDataBasic bdaNormalM5 = new BodyDataBasic(110, 20, 'm', 5);
		//Baby 0, 50, 3.2 kg
		BodyDataBasic bdaNormalM0 = new BodyDataBasic(51, 3.2, 'm', 1);
		
		/*
		 * Tests 24 jaehriger Mann
		 */
		System.out.println("\nTests 24 jaehriger Mann"); 
		System.out.println("BMI: " + bdaNormalM24.getBMI()); 
		System.out.println("Erwarteter BMI: 23.1"); 	
		System.out.println("BSA: " + bdaNormalM24.getBSA()); 
		System.out.println("Erwarteter BSA: 1.94 m2"); 	
//		System.out.println("BSA: " + bdaNormalM24.getBSAadults());
//		System.out.println("Erwarteter BSA: 1.94 m2"); 	
		System.out.println("IBW: " + bdaNormalM24.getIBW());
		System.out.println("Erwarteter IBW: 72 kg"); 	
		System.out.println("NBW: " + bdaNormalM24.getNBW()); 
		System.out.println("Erwarteter NBW: 80 kg"); 	
		
		/*
		 * Tests 24 jaehrige Frau
		 */
		System.out.println("\nTests 24 jaehrige Frau"); 
		System.out.println("BMI: " + bdaNormalF24.getBMI()); 
		System.out.println("Erwarteter BMI: 18.0"); 		
		System.out.println("BSA: " + bdaNormalF24.getBSA()); 
//		System.out.println("Erwarteter BSA: 1.67 m2"); 	
//		System.out.println("BSA: " + bdaNormalF24.getBSAadults());
		System.out.println("Erwarteter BSA: 1.67 m2"); 	
		System.out.println("IBW: " + bdaNormalF24.getIBW());
		System.out.println("Erwarteter IBW: 60 kg"); 	
		System.out.println("NBW: " + bdaNormalF24.getNBW());
		System.out.println("Erwarteter NBW: 75 kg"); 	
		
		/*
		 * Tests 20 jaehriger Mann
		 */
		System.out.println("\nTests 20 jaehriger Mann"); 
		System.out.println("BMI: " + bdaNormalM20.getBMI()); 
		System.out.println("Erwarteter BMI: 22.8"); 			
		System.out.println("BSA: " + bdaNormalM20.getBSA()); 
		System.out.println("Erwarteter BSA: 1.92 m2"); 		
//		System.out.println("BSA: " + bdaNormalM20.getBSAyoungAdults());
//		System.out.println("Erwarteter BSA: 1.92 m2"); 	
		System.out.println("IBW: " + bdaNormalM20.getIBW());
		System.out.println("Erwarteter IBW: 72 kg"); 	
		System.out.println("NBW: " + bdaNormalM20.getNBW());
		System.out.println("Erwarteter NBW: 80 kg"); 	
		
		/*
		 * Tests 5 jaehries Kind
		 */
		System.out.println("\nTests 5 jaehriger Knabe"); 
		System.out.println("BMI: " + bdaNormalM5.getBMI()); 
		System.out.println("Erwarteter BMI: 16.5"); 			
		System.out.println("BSA: " + bdaNormalM5.getBSA()); 
		System.out.println("BSA: " + "Erwarteter BSA: 0.78 m2"); 	
//		System.out.println("BSA: " + bdaNormalM5.getBSAchildren());
//		System.out.println("Erwarteter BMI: 0.78 m2"); 	
		System.out.println("IBW: " + bdaNormalM5.getIBW());
		System.out.println("Erwarteter IBW: 9 kg"); 	
		System.out.println("NBW: " + bdaNormalM5.getNBW());
		System.out.println("Erwarteter NBW: 10 kg"); 	
		
		/*
		 * Tests Baby
		 */
		System.out.println("\nTests Baby Knabe"); 
		System.out.println("BMI: " + bdaNormalM0.getBMI()); 
		System.out.println("Erwarteter BMI: 12.6"); 			
		System.out.println("BSA: " + bdaNormalM0.getBSA()); 
//		System.out.println("BSA: " + "Erwarteter BSA: 0.21  m2"); 	
//		System.out.println("BSA: " + bdaNormalM0.getBSAchildren());
		System.out.println("Erwarteter BMI: 0.21 m2"); 	
		System.out.println("IBW: " + bdaNormalM0.getIBW());
		System.out.println("Erwarteter IBW:  - 45 kg"); 	
		System.out.println("NBW: " + bdaNormalM0.getNBW());
		System.out.println("Erwarteter NBW: - 50 kg"); 
	}

}
