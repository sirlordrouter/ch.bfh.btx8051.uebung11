package ch.bfh.jg.testing;
import java.util.ArrayList;
import java.util.List;

import ch.bfh.jg.util.DueDate;


public class DueDateTester {

	/**
	 * Tester KLasse fuer DueDate. Es werden 6 verschiedene Daten ueberprueft. 
	 * Je nach Zykluslaenge kann der Geburtstermin variieren. 
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
//		GregorianCalendar cal = new GregorianCalendar(2010, 10, 21); 
//		System.out.println(cal.getTime()); 
		

		DueDate menstrualDate1 = new DueDate(21,11,2010);
		DueDate menstrualDate2 = new DueDate(7,4,2011);
		DueDate menstrualDate3 = new DueDate(9,2,2011);
		DueDate menstrualDate4 = new DueDate(9,1,2011);
		DueDate menstrualDate5 = new DueDate(8,5,2009);
		DueDate menstrualDate6 = new DueDate(17,9,2012);
		
		List<DueDate> dateList = new ArrayList<DueDate>();
		
		dateList.add(menstrualDate1);
		dateList.add(menstrualDate2);
		dateList.add(menstrualDate3);
		dateList.add(menstrualDate4);
		dateList.add(menstrualDate5);
		dateList.add(menstrualDate6);
		
		for (DueDate dueDate : dateList) {
			System.out.println("\nMenstrual date: \t\t" + dueDate.GetLastPeriodDateFormatted());
			System.out.print("Due date standard: " + dueDate.getDueDate());
			System.out.println("\tDue date Naegele: " + dueDate.getDueDateNaegele());
		}
		

		
	}

}
