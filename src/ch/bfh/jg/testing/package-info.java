/**
 * Berner Fachhochschule</br>
 * Medizininformatik BSc</br>
 * Modul 8051-HS2012</br>
 * 
 *<p>Package contains the testing classes for this project.</p>
 *
 * @author Johannes Gnaegi, gnaegj1@bfh.ch
 * @version 19.11.2012
 */
package ch.bfh.jg.testing;