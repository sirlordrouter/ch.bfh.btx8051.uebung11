package ch.bfh.jg.testing;
import java.util.ArrayList;

import ch.bfh.jg.ex11.Patient;


public class PatientTester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ArrayList<String> testPatients = new ArrayList<String>();
		ArrayList<String> testCases = new ArrayList<String>(); 
		
		testPatients.add("Muster,Petra,20.07.1982,f,Bernstr. 100,3402 Goodtown");
		testPatients.add("Meier, Max, 11.10.1976, m, Thestreet 23, 9454 Idealtown");
		
		testCases.add("1, Unfall-1, 01.01.2011, 161, 61"); 
		testCases.add("2, Unfall-2, 02.02.2011, 162, 62"); 
		testCases.add("3, Unfall-3, 03.03.2011, 163, 63"); 
		testCases.add("1, Krankheit-1, 10.10.2001, 171, 71"); 
		testCases.add("2, Unfall-1, 01.01.2010, 172, 72"); 
		
		System.out.println("Defining Patient 1: " + testPatients.get(0));
		
		String[] aCase = testPatients.get(0).split(","); 
		String aName = aCase[0];
		String aForename = aCase[1];
		String aDateOfBirth = aCase[2]; 
		char aGender = aCase[3].charAt(0); 
		
		String[] streetFull = aCase[4].split(" +"); 
		String aStreet = streetFull[0];
		String aStreetNumber = streetFull[1];
		
		String[] cityFull = aCase[5].split(" +"); 
		String aPostalCode = cityFull[0];
		String aCity = cityFull[1];
		
		Patient patient1 = new Patient(aName, aForename, aDateOfBirth, aGender);
		patient1.setAddress(aStreet, aStreetNumber, aPostalCode, aCity);
		
		System.out.println(">patient1.getPatientData(): \n" + patient1.getPatientData());
		System.out.println(">patient1.getAddress(): \n" + patient1.getAddress());
		System.out.println(">Getters: \n");
		System.out.println(">patient1.getPIN(): " + patient1.getPIN());
		System.out.println(">patient1.getName(): " + patient1.getName());
		System.out.println(">patient1.getForename(): " + patient1.getForename());
		System.out.println(">patient1.getDateOfBirth(): " + patient1.getDateOfBirth());
		System.out.println(">patient1.getGender(): " + patient1.getGender());
		System.out.println(">patient1.getPatientData(): \n" + patient1.getPatientData());		
		
		System.out.println(">patient1.setAddress(Bielstrasse, 300, 2500, Besttown): \n" );
		patient1.setAddress("Bielstrasse", "300", "2500", "Besttown");
		
		System.out.println(">patient1.setName(Meister)");
		patient1.setName("Meister"); 
		System.out.println(">patient1.getName(): " + patient1.getName());
		
		System.out.println(">patient1.getPatientData(): \n" + patient1.getPatientData());	
		
		System.out.println("patient1.defineCase(1, Unfall-1, 01.01.2011, 161, 61)");
		patient1.defineCase(1, "Unfall-1", "01.01.2011", 161, 61); 
		
		System.out.println("patient1.defineCase(2, Unfall-2, 02.02.2011, 162, 62)");
		patient1.defineCase(2, "Unfall-2", "02.02.2011", 162, 62); 
		
		System.out.println("patient1.defineCase(3, Unfall-3, 03.03.2011, 163, 63)");
		patient1.defineCase(3, "Unfall-3", "03.03.2011", 163, 63); 
		
		System.out.println(">patient1.getCase(1): \n" + patient1.getCase(1));
		System.out.println(">patient1.getCase(2): \n" + patient1.getCase(2));
		System.out.println(">patient1.getCase(3): \n" + patient1.getCase(3));
		System.out.println(">patient1.ggetCase(4): \n" + patient1.getCase(4));
		System.out.println(">patient1.getAllCases(): \n" + patient1.getAllCases());
	}

}
