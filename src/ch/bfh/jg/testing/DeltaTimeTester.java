package ch.bfh.jg.testing;
import java.util.ArrayList;

import ch.bfh.jg.util.DeltaTime;
import ch.bfh.jg.util.FormattedDate;


/**
 * Berner Fachhochschule</br> Medizininformatik BSc</br> Modul 8051-HS2012</br>
 * 
 * <p>
 * Class Description
 * </p>
 * 
 * @author Johannes Gnaegi, gnaegj1@bfh.ch
 * @version 25.09.2012
 */
public class DeltaTimeTester {

	/**
	 * Constructs DeltaTime objects, gets the time differences and prints them
	 * out.
	 */
	public static void main(String[] args) {
		int year1, month1, day1, year2, month2, day2;

		DeltaTime deltaTime;
		String referenceValue;
		String currentTime = "ct"; // Current time
		ArrayList<String> date = new ArrayList<>();
		String[] splittedTokens = new String[3];
		String[] splittedDate = new String[3];
		FormattedDate formattedDate;
		int numberOfErrors = 0;

		date.add(0, "29.06.2011   06.10.1962   48.8.23");// wrong with Std DST
		date.add(1, "06.10.1962   29.06.2011   48.8.23");// wrong with Std DST
		date.add(2, "06.05.1962   29.06.2011   49.1.23");// wrong with Std DST
		date.add(3, "29.06.2011   30.10.1962   48.7.30");// wrong with Std DST
		date.add(4, "29.06.2011   ct             0.1.6");
		date.add(5, "29.06.2010   ct             1.1.6");
		date.add(6, "06.03.2012   27.02.2012     0.0.8");
		date.add(7, "06.03.2011   27.02.2011     0.0.7");
		date.add(8, "29.06.2011   30.04.1962   49.1.29");
		date.add(9, "20.07.1964   20.07.1959     5.0.0");
		date.add(10, "31.07.1964   20.07.1959    5.0.11");
		date.add(11, "30.06.2011   30.05.2011     0.1.0");
		date.add(12, "29.06.2011   29.06.2011     0.0.0");
		date.add(13, "29.06.2011   28.06.2011     0.0.1");
		date.add(14, "29.06.2011   30.05.2011    0.0.30");
		date.add(15, "29.06.2011   30.05.1962   49.0.30");
		date.add(16, "29.06.2011   30.10.1962   48.7.30");
		date.add(17, "29.06.2011   29.12.1962    48.6.0");
		date.add(18, "01.01.2011   02.08.2011     0.7.1");
		date.add(19, "01.01.1970   31.12.1969     0.0.1");
		date.add(20, "01.01.1970   30.12.1969     0.0.2");
		date.add(21, "02.01.1970   30.12.1969     0.0.3");
		date.add(22, "29.06.1979   06.10.1962   16.8.23");
		date.add(23, "29.06.1980   06.10.1962   17.8.23");// wrong with Std DST
		date.add(24, "29.06.1960   06.10.1942   17.8.23");// wrong with Std DST
		date.add(25, "29.11.1960   06.10.1942   18.1.23");// wrong with Std DST
		date.add(26, "29.11.2000   06.10.1982   18.1.23");
		// wrong with Standard DST, introduction DST
		date.add(27, "04.04.1980   07.04.1980     0.0.3");

		for (int i = 0; i < date.size(); i++) {
			splittedTokens = date.get(i).split(" +"); // Regular Expr.
			splittedDate = splittedTokens[0].split("\\.");
			day1 = Integer.parseInt(splittedDate[0]);
			month1 = Integer.parseInt(splittedDate[1]);
			year1 = Integer.parseInt(splittedDate[2]);

			if (splittedTokens[1].compareTo(currentTime) != 0) {
				splittedDate = splittedTokens[1].split("\\.");
				day2 = Integer.parseInt(splittedDate[0]);
				month2 = Integer.parseInt(splittedDate[1]);
				year2 = Integer.parseInt(splittedDate[2]);
				referenceValue = splittedTokens[2];
				deltaTime = new DeltaTime(day1, month1, year1, day2, month2, year2);
				System.out.println("\n-Dataset: " + i + "\nDate1 defined: " + day1
						+ "." + month1 + "." + year1 + "\nDate2 defined: " + day2
						+ "." + month2 + "." + year2);
			} else {
				referenceValue = splittedTokens[2];
				deltaTime = new DeltaTime(day1, month1, year1);
				System.out.println("\nDataset: " + i + "\nDate1: " + year1 + "."
						+ month1 + "." + day1 + "\nDate2: Current time");
			}

			System.out.println("getDatesDefined():\n"
					+ deltaTime.getDatesDefined());
			System.out.println("getDeltaTimeInMs(): "
					+ deltaTime.getDeltaTimeInMs());
			System.out
					.println("getDeltaTimeInS(): " + deltaTime.getDeltaTimeInS());
			System.out.println("getDeltaTimeInMin(): "
					+ deltaTime.getDeltaTimeInMin());
			System.out.println("getDeltaTimeInHours(): "
					+ deltaTime.getDeltaTimeInHours());
			System.out.println("getDeltaTimeInDays(): "
					+ deltaTime.getDeltaTimeInDays());
			System.out.println("getDeltaTimeInYears() (rounded): "
					+ deltaTime.getDeltaTimeInYears());
			formattedDate = deltaTime.getDeltaTimeInYearsMonthsDays();
			String fromProgram = formattedDate.year + "|" + formattedDate.month
					+ "|" + formattedDate.day;
			System.out.println("getDeltaTimeInYearsMonthsDays(): " + fromProgram);

			referenceValue = referenceValue.replace('.', '|');
			System.out.println("Expected: " + referenceValue);

			if (referenceValue.compareTo(fromProgram) != 0) {
				System.out.println("****IRREGULAR!!****");
				numberOfErrors++;
			}
		}
		System.out.println("\nNumber of faults: " + numberOfErrors);
	}

}
