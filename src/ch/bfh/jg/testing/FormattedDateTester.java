package ch.bfh.jg.testing;

import ch.bfh.jg.util.FormattedDate;

/**
 * Berner Fachhochschule</br>
 * Medizininformatik BSc</br>
 * Modul 8051-HS2012</br>
 * 
 *<p>Class Description</p>
 *
 * @author Johannes Gnaegi, gnaegj1@bfh.ch
 * @version 25.09.2012
 */
public class FormattedDateTester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		String input = "20.11.1959";
		System.out.println("Input: " + input);
		FormattedDate formattedDate = new FormattedDate(input);
		System.out.println("day: " + formattedDate.day);
		System.out.println("month: " + formattedDate.month);
		System.out.println("year: " + formattedDate.year);

		System.out.println("\ndays: 23, months: 11, years: 20");
		formattedDate = new FormattedDate(23, 11, 20);
		System.out.println("days: " + formattedDate.day);
		System.out.println("months: " + formattedDate.month);
		System.out.println("years: " + formattedDate.year);
		
	}

}
