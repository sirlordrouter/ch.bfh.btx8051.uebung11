package ch.bfh.jg.testing;
import java.util.Scanner;

import ch.bfh.jg.util.BodyDataBasic;



/**
 * Berner Fachhochschule</p>
 * Medizininformatik BSc</p>
 * Modul 8051-HS2012</p>
 * 
 * Enables an User to calculate different Indices 
 * based on his size, weight and age. 
 * 
 * @author Johannes Gnaegi, gnaegj1@bfh.ch
 * @version 25.09.2012
 */
public class BodyDataBasicTesterInput {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		char gender; 
		
		Scanner in = new Scanner(System.in);
		
		if (in != null) {
			System.out.print("Geben Sie ihr Gewicht in Kg ein: ");
			double weight = in.nextDouble();
			
			System.out.print("Geben Sie ihre Koerpergroesse in cm ein: ");
			int size = in.nextInt();
				
			System.out.print("Geben Sie ihr Alter in Jahren  ein: ");
			int age = in.nextInt();
			
			System.out.print("Geben Sie ihr Geschlecht ein (m/f): ");
			String genderInput = in.next();
			if (genderInput.equalsIgnoreCase("m")) {
				gender = 'm'; 
			} else { 
				gender = 'f';
			}
			
			in.close(); 
			
			BodyDataBasic bdcNew = new BodyDataBasic(size, weight, gender, age);
			
			System.out.println("\nAus ihren Angaben erfolgt: "); 
			System.out.println("BMI: " + bdcNew.getBMI()); 	
			System.out.println("Body Surface Area (BSA): " + bdcNew.getBSA() + " m2"); 	
			System.out.println("Ideal Body Weight (IBW): " + bdcNew.getIBW() + " kg");
			System.out.println("Normal Body Weight (NBW): " + bdcNew.getNBW() + " kg"); 
		}
		
		
	}

}
