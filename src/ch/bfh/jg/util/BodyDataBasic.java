package ch.bfh.jg.util;
/**
 * Berner Fachhochschule</br>
 * Medizininformatik BSc</br>
 * Modul 8051-HS2012</br></p>
 * 
 * <p><b>Immutable Class</b></p>
 * Provides different Indices from a Person with a specified size in cm </br>
 * and a spezified weight in kg. The different indices are rounded (if necessary)</br>
 * to two digits after the point. </p>
 * 
 * @author Johannes Gnaegi, gnaegj1@bfh.ch
 * @version 20.11.2012
 * 
 */
public class BodyDataBasic {
	
	/*
	 * Not movable to Patient -> Couples BDB with Patient <->
	 */
	private final short ADULT_AGE = 22;
	private final short CHILD_AGE = 17; 
	
	/*
	 * Magic Numbers for different Indices. 
	 */
	private final double BSA_WEIGHT_FACTOR = 0.425;
	private final double BSA_SIZE_FACTOR = 0.725;
	private final double BSA_M2_KG_CM = 71.84; 
	private final double IBW_MAN_FACTOR = 0.9; 
	private final double IBW_WOMAN_FACTOR = 0.8; 
	private final byte ONE_METER_IN_CM = 100; 
		
	/*
	 * Size in cm.
	 */
	private int size;
	/*
	 * weight in kg. 
	 */
	private double weight; 
	/*
	 * Gender of the Person. 
	 */
	private char gender; 
	/*
	 * Age of the Person. 
	 * */
	private int age;
	
	/**
	 * Provides different Indices from a Person with a specified size in cm
	 * and a spezified weight in kg. The different indices are rounded (if necessary)
	 * to two digits after the point. 
	 * 
	 * @param aSize 
	 *           size of the Person in cm. (size > 50cm)
	 * @param aWeight
	 *           weight of the Person in kg (weight > 0).
	 * @param aGender
	 *           gender of the Person ( m / f)
	 * @param aAge 
	 *           age of person in years ( years > 0)
	 */
	public BodyDataBasic (int aSize, double aWeight, char aGender, int aAge) 
	{	
		/*
		 * TODO: Durch Testframework ersetzen. 
		 * Sinn ???
		 * Besser Ueberpruefen mit Testerklassen oder Pruefung der EIngaben
		 */
		assert aSize > 0;
		assert aWeight > 0;
		assert aGender == 'm' 
			|| aGender == 'M'
			|| aGender == 'f'
			|| aGender == 'F'; 
		assert aAge >= 0;
		
		size = aSize;
		weight = aWeight;				
		gender = aGender; 
		age = aAge; 
	}

	/*
	 * Returns the Body Surface Area (BSA) in m<sup>2</sup> for adults (age >= 22 years).
	 * @return Body Surface Area (BSA) in m<sup>2</sup>
	 */
	private double getBSAadults()
	{
		double bsa = (Math.pow(weight, BSA_WEIGHT_FACTOR) 
				* Math.pow(size, BSA_SIZE_FACTOR) 
				* BSA_M2_KG_CM)/10000.0;
		
		return Math.rint(bsa*100.0)/100.0; 	//Runden auf zwei Stellen. 
	}
	
	/*
	 * Returns the Body Surface Area (BSA) in m<sup>2</sup> for children (age < 17 years).
	 * @return Body Surface Area (BSA) in m<sup>2</sup>
	 */
	private double getBSAchildren()
	{
		double bsa = Math.sqrt((weight * size)/3600.0); 
		return Math.rint(bsa*100.0)/100.0; 	//Runden auf zwei Stellen.		
	}
	
	/*
	 * Returns the Body Surface Area (BSA) in m<sup>2</sup> for 
	 * young adults (17 <= age < 22 years).
	 * @return Body Surface Area (BSA) in m<sup>2</sup>
	 */
	private double getBSAyoungAdults() 
	{
		double bsa = (getBSAchildren() + getBSAadults())/2; 		
		return Math.rint(bsa*100.0)/100.0; 	//Runden auf zwei Stellen.
	}
	
	/*
	 * Returns the Ideal Body Weight (IBW) in kg (rounded) 
	 * for men according to the Broca index on the base of the size.
	 * @return Ideal Body Weight (IBW) in kg (rounded) 
	 */
	private int getIBWmen()
	{		
		double ibw = (size - ONE_METER_IN_CM) * IBW_MAN_FACTOR; 	
		return (int) Math.round(ibw); 
	}
	
	/*
	 * Returns the Ideal Body Weight (IBW) in kg (rounded) 
	 * for women according to the Broca index on the base of the size.
	 * @return Ideal Body Weight (IBW) in kg (rounded) 
	 */
	private int getIBWwomen()
	{
		double ibw = (size - ONE_METER_IN_CM) * IBW_WOMAN_FACTOR; 	
		return (int) Math.round(ibw); 
	}
	
	/**
	 * Returns the Body Mass Index (BMI) on the base of the size and the weight.
	 * BMI is rounded. 
	 * @return Body Mass Index (BMI)
	 */
	public double getBMI()
	{
		double m = size / 100.0;                           //Umwandeln von cm in m. 			
		double bmi = weight / Math.pow(m, 2);              // Formel zur Berechung des BMIs 
		return Math.rint(bmi*10.0)/10.0; 	               //Runden auf eine Stelle. 		 
	}
	
	/**
	 * Returns the Body Surface Area (BSA) in m<sup>2</sup> for the specified age. 
	 * The Formula used to calculate the BSA is according to Takahira. 
	 * @param age
	 *         age of the Person
	 * @return Body Surface Area (BSA) in m<sup>2</sup>
	 */
	public double getBSA()
	{
		if (age >= ADULT_AGE) {
			return getBSAadults();
		} else if (age < CHILD_AGE){
			return getBSAchildren();
		}
		else {
			return getBSAyoungAdults();                     //CHILD < age < ADULT
		}
		
	}
	
	/**
	 * Returns the Ideal Body Weight (IBW) in kg (rounded), 
	 * for the Gender defined in on creating the Object,
	 * according to the Broca index on the base of the size.
	 * 
	 * @return Ideal Body Weight (IBW) in kg (rounded) 
	 */
	public int getIBW()
	{
		if (gender == 'm' || gender == 'M') {
			return getIBWmen();
		} else {
			return getIBWwomen();
		}
	}
	
	/**
	 * Returns the Normal Body Weight (NBW) in kg according to the 
	 * Broca index on the base of the size.
	 * @return Normal Body Weight (NBW) in kg
	 */
	public int getNBW()
	{	
		return size-ONE_METER_IN_CM; 
	}	
}

