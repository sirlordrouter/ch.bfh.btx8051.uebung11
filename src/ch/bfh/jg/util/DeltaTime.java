package ch.bfh.jg.util;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

/**
 * Berner Fachhochschule</br>
 * Medizininformatik BSc</br>
 * Modul 8051-HS2012</br>
 * 
 * <p><b>Immutable Class</b></p>
 *<p>Class to Calculate differences in Time between two given Dates.
 *The dates can be defined both by the user. If there is just one date given, the 
 *differences are calculated with the todays date.</p>
 *
 * @author Johannes Gnaegi, gnaegj1@bfh.ch
 * @version 25.09.2012
 */
public class DeltaTime {
	
	private GregorianCalendar calendarDate1;
	private GregorianCalendar calendarDate2;
	private int year1;
	private int year2;
	private int month1;
	private int month2;
	private int date1;
	private int date2;
	private long msDate1;
	private long msDate2;

	/**
	 * Constructs a DeltaTime object on the base of a defined date and the
	 * current time.
	 * 
	 * @param aDay
	 *           Day of a month (without leading 0).
	 * @param aMonth
	 *           Month of the year (without leading 0).
	 * @param aYear
	 *           Year (without leading 0).
	 */
	public DeltaTime(int aDay, int aMonth, int aYear) {
		GregorianCalendar calDate1 = new GregorianCalendar();
		calDate1.set(GregorianCalendar.HOUR_OF_DAY, 0);
		calDate1.set(GregorianCalendar.MINUTE, 0);
		calDate1.set(GregorianCalendar.SECOND, 0);
		calDate1.set(GregorianCalendar.MILLISECOND, 0);
		GregorianCalendar calDate2 = new GregorianCalendar(aYear, aMonth - 1,
				aDay);
		buildCalendars(calDate1, calDate2);
	}

	/**
	 * Constructs a DeltaTime object on the base of two defined dates. The two
	 * dates can be defined in any order.
	 * 
	 * @param aDay1
	 *           Date 1: Day of a month (without leading 0).
	 * @param aMonth1
	 *           Date 1: Month of the year (without leading 0).
	 * @param aYear1
	 *           Date 1: Year (without leading 0).
	 * @param aDay2
	 *           Date 2: Day of a month (without leading 0).
	 * @param aMonth2
	 *           Date 2: Month of the year (without leading 0).
	 * @param aYear2
	 *           Date 2: Year (without leading 0).
	 */
	public DeltaTime(int aDay1, int aMonth1, int aYear1, int aDay2, int aMonth2,
			int aYear2) {
		GregorianCalendar calDate1 = new GregorianCalendar(aYear1, aMonth1 - 1,
				aDay1);
		GregorianCalendar calDate2 = new GregorianCalendar(aYear2, aMonth2 - 1,
				aDay2);
		buildCalendars(calDate1, calDate2);
	}

	// Sorts the two dates in order to define its calendars.
	private void buildCalendars(GregorianCalendar aCalDate1,
			GregorianCalendar aCalDate2) {
		// The Standard Time zone (id="Europe/Berlin) produces errors if the
		// date 5/6.04.1980 (introduction DST) is included in the time interval.
		aCalDate1.setTimeZone(TimeZone.getTimeZone("UTC"));
		aCalDate2.setTimeZone(TimeZone.getTimeZone("UTC"));

		if (aCalDate1.after(aCalDate2)) { // sorts the dates (date1 > date2)
			calendarDate1 = aCalDate1;
			calendarDate2 = aCalDate2;
		} else {
			calendarDate1 = aCalDate2;
			calendarDate2 = aCalDate1;
		}
		// Defines the calendars for the two dates.
		year1 = calendarDate1.get(Calendar.YEAR);
		month1 = calendarDate1.get(Calendar.MONTH) + 1;
		date1 = calendarDate1.get(Calendar.DAY_OF_MONTH);
		msDate1 = calendarDate1.getTimeInMillis();

		year2 = calendarDate2.get(Calendar.YEAR);
		month2 = calendarDate2.get(Calendar.MONTH) + 1;
		date2 = calendarDate2.get(Calendar.DAY_OF_MONTH);
		msDate2 = calendarDate2.getTimeInMillis();
	}

	/**
	 * Returns the two defined dates.
	 * 
	 * @return The two defined dates (always sorted). Format: Date1=dd.mm.yyyy
	 *         Date2=dd.mm.yyyy
	 */
	public String getDatesDefined() {
		return String.format("Date1=%02d.%02d.%04d\nDate2=%02d.%02d.%04d", date1,
				month1, year1, date2, month2, year2);
	}

	/**
	 * Returns the difference between the two dates in ms (milliseconds).
	 * 
	 * @return The difference between the two dates in ms (milliseconds).
	 */
	public long getDeltaTimeInMs() {
		return msDate1 - msDate2;
	}

	/**
	 * Returns the difference between the two dates in s (seconds).
	 * 
	 * @return The difference between the two dates in s (seconds).
	 */
	public long getDeltaTimeInS() {
		return TimeUnit.MILLISECONDS.toSeconds(msDate1 - msDate2);
	}

	/**
	 * Returns the difference between the two dates in min (minutes).
	 * 
	 * @return The difference between the two dates in min (minutes).
	 */
	public long getDeltaTimeInMin() {
		return TimeUnit.MILLISECONDS.toMinutes(msDate1 - msDate2);
	}

	/**
	 * Returns the difference between the two dates in h (hours).
	 * 
	 * @return The difference between the two dates in h (hours).
	 */
	public long getDeltaTimeInHours() {
		return TimeUnit.MILLISECONDS.toHours(msDate1 - msDate2);
	}

	/**
	 * Returns the difference between the two dates in days.
	 * 
	 * @return The difference between the two dates in days.
	 */
	public long getDeltaTimeInDays() {
		return TimeUnit.MILLISECONDS.toDays(msDate1 - msDate2);
	}

	/**
	 * Returns the difference between the two dates in years, months, days.
	 * 
	 * @return The difference between the two dates in years, months, days.
	 */
	public FormattedDate getDeltaTimeInYearsMonthsDays() {
		final int MONTHS_PER_YEAR = 12;
		int years = 0;
		int months = 0;
		int days = 0;

		if (year1 > year2)
			years = year1 - year2;

		if (month1 >= month2) {
			months = month1 - month2;
		} else {
			months = MONTHS_PER_YEAR - month2 + month1;
			years--;
		}

		if (date1 >= date2) {
			days = date1 - date2;
		} else {
			days = calendarDate2.getActualMaximum(Calendar.DAY_OF_MONTH) - date2
					+ date1;
			if (months > 0)
				months--;
		}
		return new FormattedDate(days, months, years);
	}

	/**
	 * Returns the difference between the two dates in years (rounded).
	 * 
	 * @return The difference between the two dates in years (rounded).
	 */
	public int getDeltaTimeInYears() {
		final int MIDDLE_OF_THE_YEAR = 7; // middle of the year >= July, month 7
		FormattedDate formDate = getDeltaTimeInYearsMonthsDays();

		if (formDate.month < MIDDLE_OF_THE_YEAR)
			return formDate.year;
		else
			return formDate.year + 1;
	}
}
