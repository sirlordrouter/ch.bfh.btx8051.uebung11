/**
 * Berner Fachhochschule</br>
 * Medizininformatik BSc</br>
 * Modul 8051-HS2012</br>
 * 
 *<p>Contains the utility classes for this projects.</br>
 * They calculate different indices based on given personal data.</p>
 *
 * @author Johannes Gnaegi, gnaegj1@bfh.ch
 * @version 19.11.2012
 */
package ch.bfh.jg.util;